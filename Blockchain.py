import hashlib
import time
import random

# Header (Allows classes to be written in any order so the code is structured in order from most important to least
# important rather than based off of dependencies)
class Transaction:
    pass
class Block:
    pass
class BlockChain:
    pass
class User:
    pass
# end



# Stores all the actions a user is allowed to perform and the information required to perform them.
class User:
    __publicKey: str = None
    __privateKey: str = None

    def __init__(self, publicKey: str, privateKey: str):
        self.__publicKey = publicKey
        self.__privateKey = privateKey

    def getPublicKey(self):
        return self.__publicKey

    def mine(self):
        pass

    def sendCurrency(self):
        pass




# This is the Blockchain itself
class BlockChain:
    chain = []
    transactionWaitingList = []

    def __init__(self):
        self.addBlock(Block(None))

    def addTransaction(self, transaction: Transaction):
        self.transactionWaitingList.append(transaction)

    def mineBlock(self, name: str):
        if len(self.transactionWaitingList) > 0:
            self.addBlock(Block(self.transactionWaitingList[0]))
            if random.randint(0, 2) == 1:
                self.addTransaction(Transaction("Node", name, 1))
            self.transactionWaitingList.pop(0)

    def addBlock(self, block: Block):

        # Set the index of the block
        block.index = len(self.chain)

        # If the block is not the genesis block
        if block.index > 0:
            # calculate and set the previous hash
            block.previousHash = self.hashBlock(self.chain[block.index - 1])
            print("Mining...")
            block.proof = self.proofOfWork(self.chain[block.index - 1].proof * block.index)

        self.chain.append(block)

    def hashBlock(self, block: Block):
        return hashlib.sha256(block.hashableData().encode()).hexdigest()

    # Prints the chain in human readable format
    def print(self):
        for block in self.chain:
            print(block.humanData())

    def returnWallet(self, name: str):
        wallet = 0
        for block in self.chain:
            if block.data is not None:
                if block.data.sender == name:
                    wallet -= block.data.amount
                if block.data.recipient == name:
                    wallet += block.data.amount
        return wallet

    def proofOfWork(self, lastProof):
        proof = 0
        while self.validProof(lastProof, proof) is False:
            proof += 1
        return proof

    @staticmethod
    def validProof(lastProof: str, proof: str):
        guess = str(lastProof + proof).encode()
        guessHash = hashlib.sha256(guess).hexdigest()
        return guessHash[:4] == "0000"


# This is the data that we want to immortalise in the chain
class Transaction:
    sender: str
    recipient: str
    amount: float

    def __init__(self, sender, recipient, amount):
        self.sender = sender
        self.recipient = recipient
        self.amount = amount

    # returns the block in a human readable format
    def humanData(self):
        return str("Sender: " + self.sender + " " +
                   "Recipient: " + self.recipient + " " +
                   "Amount: " + str(self.amount)
                   )

    # Returns data in most compact string form for hashing
    def hashableData(self):
        return str(self.sender) + str(self.recipient) + str(self.amount)


# This is all the data necessary to have the data stored on the chain
class Block:
    index = None
    timestamp = None
    data = None
    proof = 0
    previousHash = None

    def __init__(self, data: Transaction):
        self.timestamp = time.time()
        self.data = data

    # returns the block in a human readable format
    def humanData(self):
        if self.data is not None:
            return str("index: " + str(self.index) + " " +
                       "timestamp: " + str(self.timestamp) + " " +
                       "data: (" + self.data.humanData() + ") " +
                       "proof: " + str(self.proof) + " " +
                       "previousHash: " + str(self.previousHash) + " "
                       )
        else:
            return str("index: " + str(self.index) + " " +
                       "timestamp: " + str(self.timestamp) + " " +
                       "data: " + str(self.data) + " " +
                       "proof: " + str(self.proof) + " " +
                       "previousHash: " + str(self.previousHash) + " "
                       )

    # Returns data in most compact string form for hashing
    def hashableData(self):
        index = str(self.index)
        timestamp = str(self.timestamp)

        # Only add data if data exists
        if self.data is not None:
            data = str(self.data.hashableData())
        else:
            data = str(self.data)

        proof = str(self.proof)
        previousHash = str(self.previousHash)

        return str(index + timestamp + data + proof + previousHash)



# Testing area
names = ["Micky", "Warwick", "Ross", "James"]
def randomTransactionSimulator(blockChain: BlockChain, names: list):
    for index in range(0, 100):
        amount = random.randint(0, 100)
        sender = str(names[random.randint(0, len(names)-1)])
        reciever = str(names[random.randint(0, len(names) - 1)])
        miner = str(names[random.randint(0, len(names) - 1)])

        blockChain.addTransaction(Transaction(sender, reciever, amount))
        blockChain.mineBlock(miner)
        blockChain.mineBlock(miner)

    return blockChain


threeIsACoin = BlockChain()

randomTransactionSimulator(threeIsACoin, names)

threeIsACoin.print()

print()
print("Micky has " + str(threeIsACoin.returnWallet("Micky")) + " ThreeIsACoin's")
print("Warwick has " + str(threeIsACoin.returnWallet("Warwick")) + " ThreeIsACoin's")
print("Ross has " + str(threeIsACoin.returnWallet("Ross")) + " ThreeIsACoin's")
print("James has " + str(threeIsACoin.returnWallet("James")) + " ThreeIsACoin's")
print("Node has given out " + str(threeIsACoin.returnWallet("Node") * -1) + " ThreeIsACoin's")

print("Number of mined blocks: " + str(len(threeIsACoin.chain)))
print("Number of pending transactions: " + str(len(threeIsACoin.transactionWaitingList)))
